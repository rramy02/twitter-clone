Rails.application.routes.draw do
  resources :tweets
  devise_for :users
  get 'home/index'
  root to: "home#index"
end
